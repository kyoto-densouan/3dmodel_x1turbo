# README #

1/3スケールのSHARP X-1 turbo風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。

***

# 実機情報

## メーカ
- シャープ

## 発売時期
- X1turbo 1984年10月
- X1turbo model40 1985年7月
- X1turbo II 1985年11月

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/X1_(%E3%82%B3%E3%83%B3%E3%83%94%E3%83%A5%E3%83%BC%E3%82%BF))
- [懐かしのパソコン](https://greendeepforest.com/?p=869)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_x1turbo/raw/79024aaa3c4583152658b065bf2e265349bcfeeb/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_x1turbo/raw/79024aaa3c4583152658b065bf2e265349bcfeeb/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_x1turbo/raw/79024aaa3c4583152658b065bf2e265349bcfeeb/ExampleImage.png)
